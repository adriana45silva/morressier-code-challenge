import {PosterStore, SearchStore} from 'typescript/stores/index'
import {History, createBrowserHistory} from 'history'
import bowser from 'bowser'
import {action, observable, computed} from 'mobx'
import queryString from 'query-string'


export default class RootStore {
  public posters = new PosterStore(this)
  public search = new SearchStore(this)
  public bowser = bowser ? bowser : {}

  @observable loading: boolean = false
  @observable config = process.env.config as any

  @action handleError(err: any) {
    if (err && err.status) {
      return "Service unavailable at this moment"
    }
  }  

  
  constructor(public history: History = createBrowserHistory()){
    this.search.updateQuery(this.search.query)
    
    history.listen(() => {
      this.search.updateQuery(this.search.query)
    })
  }
}
