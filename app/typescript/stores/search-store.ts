import {action, observable, computed} from 'mobx'
import { runInAction} from 'mobx'
import { SearchResultsResponse } from 'typescript/models/index'
import {RootStore} from 'typescript/stores/index'
import queryString from 'query-string'

export default class SearchStore {
  constructor(private root: RootStore){
    this.root = root
  }

  @observable parsed: any = queryString.parse(this.root.history.location.search)
  @observable query: string = queryString.parse(this.root.history.location.search).query || ''
  @observable offset: number = parseInt(queryString.parse(this.root.history.location.search).offset) || 0
  @observable searchResponse: SearchResultsResponse 

  @action checkHistory() {
    let parsed = queryString.parse(this.root.history.location.search)

    if (parsed && Object.keys(parsed).length) {
      return this.parsed = parsed
    }
  }

  @action updateQuery (newVal: string = ''){
    this.query = encodeURI(newVal)
    this.checkHistory()
  }

  @action updatePage() {
    this.offset = Number(this.offset+1)
  }

  @action setPage(currentPage: number) {
    this.offset = Number(currentPage)
  }

  @action handleError(err: any) {
    if (err && err.status) {
      return "Service unavailable at this moment"
    }
  }  

  @action async searchForPosters(query: string = '', offset: number, limit: number = 10) {
    this.root.loading = true
    let request = new Request(`/v3/posters/search?query=${query}&offset=${offset}&limit=${limit}`, {
      method: 'GET',
      headers: { 'authorization': `bearer ${this.root.config.API_TOKEN}` }
    })

    try {
      let req = await fetch(request)
      if (req.ok && req.status == 200){
        let result = await req.json()
        runInAction(() => {
          this.searchResponse = result
          this.root.loading = false
        })
      } else {
        throw req
      }
    } catch (err) {
     this.root.handleError(err)
    }
  }

  @computed get posters () {
    return this.searchResponse ? this.searchResponse.posters : null
  }

  @computed get events () {
    return this.searchResponse ? this.searchResponse.events : null
  }  

  @computed get users () {
    return this.searchResponse ? this.searchResponse.users : null
  }   
  
  @computed get collection () {
    return this.searchResponse ? this.searchResponse.collection : null
  }

  @computed get totalItems () {
    return this.searchResponse ? this.searchResponse.collection.total : null
  }
}