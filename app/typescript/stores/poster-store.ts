import {action, observable, computed} from 'mobx'
import {runInAction} from 'mobx'
import { PosterResponse} from 'typescript/models/index'
import {RootStore} from 'typescript/stores/index'

export default class PosterStore {
  constructor(private root: RootStore){
    this.root = root
  }

  @observable singlePostResponse: PosterResponse

  @observable posterId = this.root.history.location.pathname.replace(/\/\posters\//g, '')

  @computed get title () {
    return this.singlePostResponse ? this.singlePostResponse.poster.title : null
  }

  @computed get imgPath () {
    return this.singlePostResponse ? this.singlePostResponse.poster.thumb_url_medium : null
  }  

  @computed get event () {
    return this.singlePostResponse ? this.singlePostResponse.event.name : null
  }    

  @computed get keywords () {
    return this.singlePostResponse ? this.singlePostResponse.poster.keywords : null
  } 

  @computed get author () {
    return this.singlePostResponse ? this.singlePostResponse.users[0].full_name : null
  }   

  @computed get abstract () {
    return this.singlePostResponse ? this.singlePostResponse.poster.paper_abstract : null
  }     

  @action async fetchPoster(posterId: string) {
    this.root.loading = true
    let request = new Request(`/v2/posters/${posterId}`, {
      method: 'GET'
    })

    try {
      let req = await fetch(request)
      if (req.ok && req.status == 200){
        let result = await req.json()
        runInAction(() => {
          this.singlePostResponse = result
          this.root.loading = false
        })
      } else {
        throw req
      }
    } catch (err) {
     this.root.handleError(err)
    }
  }

}