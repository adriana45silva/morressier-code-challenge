import "promise/polyfill"
import "whatwg-fetch"
import 'stylesheets/index'
import React from 'react'
import {render} from 'react-dom'
import {AppContainer} from 'react-hot-loader'
import {App} from 'typescript/app'
import {Provider} from 'mobx-react'
import {RootStore} from 'typescript/stores/index'
import 'stylesheets/index'

const root = document.getElementById('app')
const store = new RootStore()

// Render the component initially
render(
  <AppContainer>
    <Provider {...store}>
      <App rootStore={store}/>
    </Provider>
  </AppContainer>,
  root
)

// Render the component in case of a hot replacement
// This is removed automatically in production
if (module.hot) {
  module.hot.accept('./app', () => {
    const NextApp = require('./app').default

    render(
      <AppContainer>
        <Provider {...store}>
          <NextApp rootStore={store}/>
        </Provider>
      </AppContainer>,
      root
    )
  })
}
