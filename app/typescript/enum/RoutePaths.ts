enum RoutePaths {
  Home = '/',
  Search = '/search',
  Poster = '/posters'
}

export default RoutePaths
