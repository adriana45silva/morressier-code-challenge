import React, {SFC} from 'react'
import InlineSVG from 'svg-inline-react'
import {observer} from 'mobx-react'
import RoutePaths from 'typescript/enum/RoutePaths'
const logo = require('../../../assets/logo.png')

interface HeaderComponentProps {
  stores?: any
  query?: any
}

export const HeaderComponent: SFC<HeaderComponentProps> = observer(({stores, query}) => {

  const searchAndPush = () => {
    stores.root.history.push(`${RoutePaths.Search}?query=${stores.query}&offset=${stores.offset}`)
    stores.searchForPosters(stores.query, stores.offset)
  }
  
  return (
    <header className="mr__header">
      <ul className="mr__header__nav">
        <li className="mr__header__nav__items" >
            <a href={RoutePaths.Home}><img src={logo} /></a>
        </li>
        <li className="mr__header__nav__items" >
          <div>
            <input type="text" placeholder="Search here" value={decodeURIComponent(query)} onChange={evt => stores.updateQuery(evt.target.value)}/>
            <button onClick={() => searchAndPush()}><i className="fas fa-search"></i></button>
          </div>
        </li> 
      </ul>
    </header>
  )
})