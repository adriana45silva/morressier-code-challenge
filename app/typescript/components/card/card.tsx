import React, {SFC} from 'react'
import {observer} from 'mobx-react'
import {Event} from 'typescript/models/index'
import {Link} from 'react-router-dom'
import RoutePaths from 'typescript/enum/RoutePaths'

interface CardComponentProps {
  title?: string
  author?: string | Array<string>
  event?: string | Array<Event>
  keywords?: Array<string>
  imgPath?: string
  stores?: any
  id?: string
}

export const CardComponent: SFC<CardComponentProps> = observer(({title, author, event, keywords, stores, imgPath, id}) => {
  // const {loading} = stores.root
  

  return (
    <div className="mr__card card" >
    <img className="card-img-top" src={imgPath} />
    <div className="card-body">
      <h5 className="card-title">
        <Link to={`${RoutePaths.Search}?query=${encodeURI(title)}&offset=0`}>{title}</Link>
      </h5>
      <h6 className="card-subtitle mb-2 text-muted">
        <Link to={`${RoutePaths.Search}?query=${encodeURI(author)}&offset=0`}>{author}</Link>
      </h6>
      <p className="card-text">
        <Link  to={`${RoutePaths.Search}?query=${encodeURI(event)}&offset=0`}>{event}</Link>
      </p>
      <Link to={`${RoutePaths.Poster}/${id}`} className="btn btn-primary">More</Link>
    </div>
    <div className="card-footer text-muted">
      {
        keywords && keywords.map((el, i) => {
          return (
          <span key={i} className="mr__card__keywords badge badge-secondary">
            <Link to={`${RoutePaths.Search}?query=${encodeURI(el)}&offset=0`}>
              {el}            
            </Link>
          </span>)
        })
      }
    </div>    
  </div>    
  )   
})