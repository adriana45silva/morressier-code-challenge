import React from 'react'
import {ErrorComponent} from './error'
import {shallow} from 'enzyme'

describe('LoadingButtonComponent', () => {

  let handleError = (err: any) => {
    if (err && err.status === 503 || !err) {
      return "Service unavailable at this moment"
    }
  } 

  let err = {
    status: 503
  }

  let wrapper = shallow(
    <ErrorComponent checkPropertyError={null} handleError={() => handleError(err)}/>
  )


  it('should be defined', () => {
    expect(ErrorComponent).toBeDefined();
  })

  it('should render error message', () => {
    expect(wrapper.find('span').text()).toBe("Service unavailable at this moment")
  })
})