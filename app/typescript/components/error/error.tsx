import React, {Fragment, SFC} from 'react'
import {observer} from 'mobx-react'
import { RootStore } from 'typescript/stores/index';

interface ErrorComponentProps {
  checkPropertyError: boolean
  handleError: (prop:any) => string
}

export const ErrorComponent: SFC<ErrorComponentProps> = observer(({checkPropertyError, handleError}) => {

  return (
    <Fragment>
        <p className="fr__error">
          <i className="fas fa-exclamation-triangle"></i>
          <span>{handleError(checkPropertyError)}</span>
        </p>      
    </Fragment>
  )   
})