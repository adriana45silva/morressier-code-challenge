import React, {SFC} from 'react'
import {observer} from 'mobx-react'

export const WelcomeComponent: SFC = observer(() => {

  return (
    <section className="mr__welcome">
      <h1>Welcome!</h1>
      <p>
        Use the search bar to find a poster!
      </p>
    </section>
  )   
})