import React, {SFC} from 'react'
import {observer} from 'mobx-react'

interface LoadingComponentProps {
  loading: boolean
}

export const LoadingComponent: SFC<LoadingComponentProps> = observer(({loading}) => {

  return (
    <section className={loading ? 'mr__loading' : 'mr__loading hidden'}>
      <h1>
        <i className={"fas fa-circle-notch fa-spin"}></i>
        <span> Loading...</span>
        </h1>
    </section>
  )   
})