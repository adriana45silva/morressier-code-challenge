import React, {SFC} from 'react'
import {observer} from 'mobx-react'
import {Event} from 'typescript/models/index'

interface PosterComponentProps {
  title?: string
  author?: string | Array<string>
  event?: string | Array<Event>
  keywords?: Array<string>
  imgPath?: string
  stores?: any
  id?: string
  abstract?: string
}

export const PosterComponent: SFC<PosterComponentProps> = observer(({title, author, event, keywords, stores, imgPath, abstract}) => {

  return (
    <section className="mr__poster">
      <img src={imgPath} className="mr__poster__img"/>
      <div className="mr__poster__desc">
        <h2 className="mr__poster__desc__title">{title}</h2><br/>
        <h3 className="mr__poster__desc__author">{author}</h3>
        <h4 className="mr__poster__desc__event">{event}</h4>
        <p className="mr__poster__desc__abstract">{abstract}</p>

        <p className="mr__poster__desc__keywords">
          {keywords && keywords.map((el,i) => <span key={i}>{el}</span>)}          
        </p>
      </div>
    </section>
  )   
})