import React, {Fragment, SFC} from 'react'
import {observer} from 'mobx-react'
import RoutePaths from 'typescript/enum/RoutePaths'
import {RootStore} from 'typescript/stores/index'
import {Router, Route, Switch} from 'react-router-dom'
import {SearchPage, PostersPage} from 'typescript/pages/index';
import {HeaderComponent, WelcomeComponent, LoadingComponent} from 'typescript/components/index'

interface AppProps {
  rootStore?: RootStore
}

export const App: SFC<AppProps> = observer(({rootStore, ...props}) => {

  return (    
    <Fragment>
      <HeaderComponent query={rootStore.search.query} stores={rootStore.search} />
      <LoadingComponent loading={rootStore.loading}/>
      <Router history={rootStore.history}>
        <Switch>    
          <Route exact path={RoutePaths.Home} component={WelcomeComponent}/>
          <Route path={RoutePaths.Search} component={SearchPage}/>
          <Route path={RoutePaths.Poster} component={PostersPage}/>          
        </Switch>
      </Router>
    </Fragment>
  )
})

