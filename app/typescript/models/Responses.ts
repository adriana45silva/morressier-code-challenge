import {Poster} from './Poster';
import {Collection} from './Collection';
import {Event} from './Event';
import {User} from './User';

export interface SearchResultsResponse {
  collection: Collection,
  events: Array<Event>,
  posters: Array<Poster>,
  users: Array<User>
}

export interface PosterResponse {
  poster: Poster,
  event: Event,
  users: Array<User>
}
