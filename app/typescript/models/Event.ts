export interface Event {
  end_date?: string,
  id: string,
  language?: string,
  location?: string,
  name: string,
  short_name: string,
  start_date?: string,
  venue?: string,
  website_url?: string
  poster_config?: any
};