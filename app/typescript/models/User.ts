export interface User {
  id: string,
  title?: string,
  full_name: string,
  picture_url?: string,
  is_activated: boolean,
  organization?: string,
  department?: string,
  organization_location?: string,
  public_profile?: boolean
};