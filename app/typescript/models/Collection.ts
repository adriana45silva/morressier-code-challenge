export interface Collection {
  limit: number,
  offset: number,
  total: number,
  items: Array<string>
};
