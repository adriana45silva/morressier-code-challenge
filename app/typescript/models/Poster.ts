import {Event} from './Event';
export interface Poster {
  authors: Array<string>,
  author_names?: Array<string>,
  fileid?: string
  filetype?: string
  event: string,
  id: string,
  keywords?: Array<string>,
  score?: number
  paper_abstract?: string,
  public_access_enabled?: boolean,
  submission_completed?: boolean,
  title: string,
  uploaded_at: string,
  thumb_url: string,
  thumb_url_medium: string,
  thumb_url_large: string
  _co_authors?: any
  _legacy_abstract_id?: any
  _legacy_id?: number
  embargoed?: boolean
  processing_state?: any
  stats?: any
  uploader?: any
}