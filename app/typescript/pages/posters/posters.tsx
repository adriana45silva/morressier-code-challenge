import React, {Component} from 'react'
import {inject, observer,} from 'mobx-react'
import {PosterStore, SearchStore, RootStore} from 'typescript/stores/index'
import { CardComponent, PosterComponent } from 'typescript/components/index';
import {Link} from 'react-router-dom'
import RoutePaths from 'typescript/enum/RoutePaths'
interface PostersPageProps {
  posterStore?: PosterStore
  root?: RootStore
}

@inject((root: RootStore) => ({
  posterStore: root.posters,
  root
}))


@observer
export class PostersPage extends Component<PostersPageProps> {

  componentWillMount(){
    const {root, posterStore} = this.props
    let posterId = root.history.location.pathname.replace(/\/\posters\//g, '')

    posterStore.fetchPoster(posterId)
  }

  render() {
    const {root, posterStore} = this.props

    return (
      <section className={root.loading ? 'mr__list hidden' : 'mr__list'}>
        <PosterComponent 
          imgPath={posterStore.imgPath} 
          author={posterStore.author} 
          event={posterStore.event} 
          keywords={posterStore.keywords} 
          abstract={posterStore.abstract} 
          title={posterStore.title}
        />
      </section>
    )
  }
}