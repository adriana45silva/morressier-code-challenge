import React, {Component} from 'react'
import {inject, observer,} from 'mobx-react'
import {PosterStore, SearchStore, RootStore} from 'typescript/stores/index'
import { CardComponent } from 'typescript/components/index';
import {Link} from 'react-router-dom'
import RoutePaths from 'typescript/enum/RoutePaths'
import queryString from 'query-string'
interface SearchPageProps {
  searchStore?: SearchStore
  root?: RootStore
}

interface SearchPageState {
  queryParams: any
}

@inject((root: RootStore) => ({
  searchStore: root.search,
  root
}))


@observer
export class SearchPage extends Component<SearchPageProps, SearchPageState> {

  constructor(props){
    super(props)
    const {root} = this.props
    this.state = {
      queryParams: queryString.parse(root.history.location.search)
    }
  }

  componentWillMount(){
    const {searchStore} = this.props
    searchStore.searchForPosters(searchStore.query, searchStore.offset)
  }

  componentWillUpdate(){
    const {searchStore} = this.props
    const {queryParams} = this.state
    if (searchStore.parsed.query !== queryParams.query || searchStore.parsed.offset !== queryParams.offset) {
      window.scrollTo(0,0)
      searchStore.setPage(searchStore.parsed.offset)
      searchStore.updateQuery(searchStore.parsed.query)
      this.setState({
        queryParams: searchStore.parsed
      })
      searchStore.searchForPosters(searchStore.query, searchStore.offset) 
    }
  }

  render() {
    const {searchStore, root} = this.props

    let forward = searchStore.offset + 1
    let back = searchStore.offset == 0 ? searchStore.offset : searchStore.offset - 1

    return (
      <section className={root.loading ? 'mr__list hidden' : 'mr__list'}>
        <div className="mr__list__container">
          {
            searchStore.posters ?
              searchStore.posters.map((el, i) => {
                let eventName = searchStore.events.filter(e => e.id === el.event ? e.name : null)
                return <CardComponent key={i} id={el.id} title={el.title} author={el.author_names} keywords={el.keywords} event={eventName[0].name} imgPath={el.thumb_url} stores={searchStore} />
              })
            : null
          } 
          {
            root && searchStore && (
              <nav className="mr__list__container__navigation">
                <Link 
                  to={`${RoutePaths.Search}?query=${(searchStore.query)}&offset=${back}`} 
                  className={`${searchStore.offset !== 0 ? '' : 'disabled'} btn btn-primary`}>
                    <i className="fas fa-arrow-left"></i>
                </Link>
                <Link 
                  to={`${RoutePaths.Search}?query=${(searchStore.query)}&offset=${forward}`} 
                  className={`${ searchStore.totalItems && searchStore.offset < searchStore.totalItems - 1 ? '' : 'disabled'} btn btn-primary`}>
                    <i className="fas fa-arrow-right"></i>
                  </Link>                
              </nav>            
            )
          }                 
        </div>
      </section>
    )
  }
}